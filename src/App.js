import React from 'react';
import {BrowserRouter as Router, Switch, Route, Redirect} from "react-router-dom";

import 'bootstrap/dist/css/bootstrap.min.css';
import UserLogin from './components/HomeSection/HomeUserLogin';
import UserRegister from './components/HomeSection/HomeUserRegister';
import BoardsFeed from './components/Boards/BoardsFeed';
import MyProfile from './components/Profile/Profile';
import Gdpr from './components/Gdpr/Gdpr'
import MyAvatar from "./components/Avatar/MyAvatar";
import Board from "./components/Boards/Board";
import {history} from './_helpers';

function App() {

    return (
        <div>
            <Router history={history}>
                <Switch>
                    <Route exact path={'/'} component={() =>
                        <UserLogin/>
                    }/>
                    <Route path={"/login"} component={UserLogin}/>
                    <Route path={"/register"} component={UserRegister}/>
                    <Route exact path={"/boards"} component={BoardsFeed}/>
                    <Route path={"/boards/:id"} component={Board} />
                    <Route path={"/profile"} component={MyProfile}/>
                    <Route path={"/gdpr"} component={Gdpr}/>
                    <Route path={"/avatar"} component={MyAvatar}/>
                    <Redirect from="*" to="/"/>
                </Switch>
            </Router>
        </div>
    );
}


export default App;
