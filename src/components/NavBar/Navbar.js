import React from "react";
import "./NavBar.css";
import { Nav, Navbar } from "react-bootstrap";

export default function NavBar() {
    return (
        <Navbar collapseOnSelect expand="lg" bg="dark" variant="dark" id="navBar" sticky="top">
            <Navbar.Brand href="/home">My Trello</Navbar.Brand>
            <Navbar.Toggle aria-controls="responsive-navbar-nav" />
            <Navbar.Collapse id="responsive-navbar-nav">
                <Nav className="mr-auto">
                    <Nav.Link href="/login" id="volunteer">Login</Nav.Link>
                    <Nav.Link href="/register" id="volunteer">Register</Nav.Link>
                </Nav>
            </Navbar.Collapse>
        </Navbar>
    );
}
