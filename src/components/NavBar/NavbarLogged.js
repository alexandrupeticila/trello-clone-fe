import React, {useEffect, useState} from "react";
import {Nav, Navbar, Row} from "react-bootstrap";
import axios from "axios";
import './NavBarLogged.css'

export default function NavBarLogged() {
    const [user, setUser] = useState("");
    const [photo, setPhoto] = useState('');

    function getUser() {
        const token = localStorage.getItem("token");
        axios.get(`/user/profile`, {
            headers: {
                Authorization: `Bearer ${token}`
            }
        })
            .then(response => {
                const jsonResponse = JSON.parse(JSON.stringify(response));
                setUser(jsonResponse.data)
                console.log(user)
            })
            .catch(error => {
                //alert(error);
            })

        axios.get(`/user/profile/photo`, {
            headers: {
                Authorization: `Bearer ${token}`
            }
        })
            .then(response => {
                const base64Image = response.data;
                setPhoto(base64Image)
            })
            .catch(error => {
            })

    }

    useEffect(() => {
        getUser();
    }, [])

    return (
        <Navbar collapseOnSelect expand="lg" bg="dark" variant="dark" id="navBar" sticky="top">
            <Navbar.Brand href="/boards">Boards</Navbar.Brand>
            <Navbar.Toggle aria-controls="responsive-navbar-nav"/>
            <Navbar.Collapse id="responsive-navbar-nav">
                <Nav className="ml-auto">
                    <Nav.Link href="/profile">
                        <Row>
                            <img className="user-photo" src={`data:image/svg+xml;utf8,${encodeURIComponent(photo)}`}
                                 alt=""/>
                            <div>Hi, {user.firstName}</div>
                        </Row>
                    </Nav.Link>
                    {/*<Nav.Link href="/login"><i className="fas fa-sign-out-alt fa-2x"></i></Nav.Link>*/}
                </Nav>
            </Navbar.Collapse>
        </Navbar>
    );
}
