import React, {useCallback, useEffect, useState} from "react";
import {Button, Card, Container, Form, Modal, Row} from "react-bootstrap"
import axios from 'axios';
import NavBarLogged from '../NavBar/NavbarLogged'
import {ToastContainer} from 'react-toastify';
import "./Feed.css"
import BoardView from "./BoardView";

function AddBoardPopup(props) {
    const colors = ['green', 'red', 'pink', 'blue', 'orange', 'black']
    const [color, setColor] = useState('');
    const [text, setText] = useState('');

    const onChangeColor = useCallback((event) => {
        setColor(event.target.value)
    }, []);

    const onChangeText = event => {
        setText(event.target.value)
    }
    const addBoard = (event) => {
        const token = localStorage.getItem("token");
        const board = {
            color: color,
            name: text,
            stared: true
        }

        axios.post("/boards/create", board, {
            headers: {
                Authorization: `Bearer ${token}`
            }
        })
            .then(response => {
                props.getBoards()
                props.onHide();
            })
            .catch(error => {
                console.log(error);
                props.onHide();
            })
    }

    return (
        <>
            <Modal
                {...props}
                size="lg"
                aria-labelledby="contained-modal-title-vcenter"
                centered
            >
                <Modal.Header closeButton>
                    <Modal.Title id="contained-modal-title-vcenter" style={{alignItems: 'center'}}>
                        Give your board a name and select a color!
                    </Modal.Title>
                </Modal.Header>
                <Modal.Body>
                    <Form>
                        <Form.Group controlId="username">
                            <Form.Label>Name</Form.Label>
                            <Form.Control
                                type="text"
                                placeholder="board's name"
                                onChange={onChangeText}
                            />
                        </Form.Group>
                        <Form.Group>
                            <Form.Control
                                as="select"
                                custom
                                onChange={onChangeColor}
                                className="input-text"
                            >
                                <option value="" disabled selected hidden>
                                    Please Choose...
                                </option>
                                {colors.map((name, index) => (
                                    <option key={index}>{name}</option>
                                ))}
                            </Form.Control>
                        </Form.Group>
                    </Form>
                </Modal.Body>
                <Modal.Footer>
                    <Button variant="info" onClick={addBoard}>Create</Button>
                    <Button variant="danger" onClick={props.onHide}>Cancel</Button>
                </Modal.Footer>
            </Modal>
        </>
    );
}

function BoardsFeed(props) {
    const [boards, setBoards] = useState([]);
    const [createPopup, setCreatePopup] = useState(false);

    function getBoards() {
        const token = localStorage.getItem("token");
        axios.get(`boards/getAll`, {
            headers: {
                Authorization: `Bearer ${token}`
            }
        })
            .then(response => {
                const jsonResponse = JSON.parse(JSON.stringify(response));
                setBoards(jsonResponse.data)

            })
            .catch(error => {
                console.log(error);
            })
    }

    useEffect(() => {
        getBoards();
    }, [])

    const addBoard = (event) => {
        setCreatePopup(true)
    }

    const deleteBoard = (id) => {
        const token = localStorage.getItem("token");
        axios.delete(`boards/delete/${id}`, {
            headers: {
                Authorization: `Bearer ${token}`
            }
        })
            .then(response => {
                getBoards();
            })
            .catch(error => {
                console.log(error);
            })
    }

    return (
        <>
            <ToastContainer
                position="top-center"
                autoClose={2000}
                hideProgressBar={false}
                newestOnTop
                closeOnClick
                rtl={false}
                draggable
                pauseOnHover
            />
            <NavBarLogged/>
            <Container style={{width: "100%"}} className="board-container">
                < Row witdh="100%" className="justify-content-md-right">
                </Row>
                <AddBoardPopup
                    show={createPopup}
                    onHide={() => setCreatePopup(false)}
                    getBoards={() => getBoards()}
                />
                <Row className="post-content">
                    {boards.map(el =>
                        <BoardView el={el} getBoards={() => getBoards()} deleteBoard={deleteBoard}/>
                    )}
                    <div className="col-board-wrapper">
                        <Card className="post-content-card" style={{
                            width: "300px", backgroundColor: "lightgrey",
                            height: "150px"
                        }} onClick={addBoard}>
                            <i className="fas fa-plus plus-icon fa-3x"/>
                        </Card>
                    </div>

                </Row>
            </Container>
        </>
    )
}

export default BoardsFeed;