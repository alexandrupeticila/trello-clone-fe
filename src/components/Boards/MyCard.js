import React, {useState} from 'react';
import axios from 'axios';
import {Button, Card, Col, Form, Modal, Row} from "react-bootstrap";
import "./Board.css"

function ShowCardPopup(props) {

    return (
        <>
            <Modal
                {...props}
                size="lg"
                aria-labelledby="contained-modal-title-vcenter"
                centered
            >
                <Modal.Header closeButton>
                    <Modal.Title id="contained-modal-title-vcenter" style={{alignItems: 'center'}}>
                        {props.card.title}
                    </Modal.Title>
                </Modal.Header>
                <Modal.Body>
                    <Card>
                        <Card.Body
                            style={{fontSize: 18}}
                        >{props.card.description}</Card.Body>
                    </Card>
                </Modal.Body>
                <Modal.Footer>
                    <Button variant="danger" onClick={props.onHide}>Close</Button>
                </Modal.Footer>
            </Modal>
        </>
    );
}

function EditCardPopup(props) {
    const [title, setTitle] = useState(props.card.title);
    const [description, setDescription] = useState(props.card.description);

    const onChangeText = event => {
        setTitle(event.target.value)
    }
    const onChangeDescription = event => {
        setDescription(event.target.value)
    }
    const addCardsList = (event) => {
        const token = localStorage.getItem("token");
        const card = {
            title: title,
            description: description,
        }

        axios.put(`/boards/editCard/${props.id}/${props.listId}/${props.cardId}`, card, {
            headers: {
                Authorization: `Bearer ${token}`
            }
        })
            .then(response => {
                props.getListCards()
                props.onHide();
            })
            .catch(error => {
                console.log(error);
                props.onHide();
            })
    }

    return (
        <>
            <Modal
                {...props}
                size="lg"
                aria-labelledby="contained-modal-title-vcenter"
                centered
            >
                <Modal.Header closeButton>
                    <Modal.Title id="contained-modal-title-vcenter" style={{alignItems: 'center'}}>
                        Edit card!
                    </Modal.Title>
                </Modal.Header>
                <Modal.Body>
                    <Form>
                        <Form.Group controlId="username">
                            <Form.Label>New title</Form.Label>
                            <Form.Control
                                type="text"
                                placeholder={props.card.title}
                                onChange={onChangeText}
                                value={title}
                            />
                        </Form.Group>
                        <Form.Group controlId="username">
                            <Form.Label>Description</Form.Label>
                            <Form.Control
                                as="textarea"
                                row="3"
                                placeholder={props.card.description}
                                onChange={onChangeDescription}
                                value={description}
                            />
                        </Form.Group>
                    </Form>
                </Modal.Body>
                <Modal.Footer>
                    <Button variant="info" onClick={addCardsList}>Update</Button>
                    <Button variant="danger" onClick={props.onHide}>Cancel</Button>
                </Modal.Footer>
            </Modal>
        </>
    );
}

function MyCard(props) {
    const [editCardPopup, setEditCardPopup] = useState(false)
    const [showCardPopup, setShowCardPopup] = useState(false)

    const editCard = () => {
        setEditCardPopup(true)
    }

    const showCard = () => {
        setShowCardPopup(true)
    }

    const deleteCard = () => {
        const token = localStorage.getItem("token");
        axios.delete(`/boards/delete/${props.id}/${props.listId}/${props.cardId}`, {
            headers: {
                Authorization: `Bearer ${token}`
            }
        })
            .then(response => {
                props.getListCards();
            })
            .catch(error => {
                console.log(error);
            })
    }

    return (
        <Row className="card-section">
            <Col className="col-card-trash">
                <i className="far fa-trash-alt" onClick={deleteCard}/>
            </Col>
            <Col className="col-card-title">
                <ShowCardPopup
                    show={showCardPopup}
                    onHide={() => setShowCardPopup(false)}
                    card={props.card}
                />
                <div onClick={showCard}
                     style={{color: "black"}}
                >{props.card.title}</div>

            </Col>
            <Col className="col-card-edit">
                <EditCardPopup
                    show={editCardPopup}
                    onHide={() => setEditCardPopup(false)}
                    getListCards={() => props.getListCards()}
                    id={props.id}
                    listId={props.listId}
                    cardId={props.card.id}
                    card={props.card}
                />
                <i className="far fa-edit" onClick={editCard}/>
            </Col>
        </Row>

    )
}


export default MyCard;
