import React, {useState} from "react";
import {Button, Card, Form, Modal, Row} from "react-bootstrap"
import axios from 'axios';
import "./Feed.css"
import {Link} from "react-router-dom";

function EditBoardPopup(props) {
    const [text, setText] = useState('');


    const onChangeText = event => {
        setText(event.target.value)
    }

    const editBoard = (event) => {
        console.log("--------------------")
        console.log(props.id);
        const token = localStorage.getItem("token");
        const board = {
            name: text,
        }

        axios.put(`/boards/edit/${props.id}`, board, {
            headers: {
                Authorization: `Bearer ${token}`
            }
        })
            .then(response => {
                props.getBoards()
                props.onHide();
            })
            .catch(error => {
                console.log(error);
                props.onHide();
            })
    }

    return (
        <>
            <Modal
                {...props}
                size="lg"
                aria-labelledby="contained-modal-title-vcenter"
                centered
            >
                <Modal.Header closeButton>
                    <Modal.Title id="contained-modal-title-vcenter" style={{alignItems: 'center'}}>
                        Edit the name of your board!
                    </Modal.Title>
                </Modal.Header>
                <Modal.Body>
                    <Form>
                        <Form.Group controlId="username">
                            <Form.Label>New name</Form.Label>
                            <Form.Control
                                type="text"
                                placeholder="board's new name"
                                onChange={onChangeText}
                            />
                        </Form.Group>
                    </Form>
                </Modal.Body>
                <Modal.Footer>
                    <Button variant="info" onClick={editBoard}>Save</Button>
                    <Button variant="danger" onClick={props.onHide}>Cancel</Button>
                </Modal.Footer>
            </Modal>
        </>
    );
}

function BoardView(props) {
    const [editPopup, setEditPopup] = useState(false);

    const editBoard = (event) => {
        setEditPopup(true)
    }
    return (
        <>
            <div className="col-board-wrapper">
                <Card className="post-content-card" style={{
                    width: "300px", backgroundColor: props.el.color,
                    height: "150px"}}>
                    <Card.Title style={{maxWidth: "200px"}}>
                        <Link to={`/boards/${props.el.id}`} style={{color: "white"}}>{props.el.name}</Link>
                    </Card.Title>
                    <Card.Footer className="post-footer">
                        <Row>
                            <div className="trash-icon">
                                <i className="far fa-trash-alt" onClick={() => props.deleteBoard(props.el.id)}/>
                            </div>
                            <div className="edit-icon">
                                <i className="far fa-edit" onClick={editBoard}/>
                                <EditBoardPopup
                                    show={editPopup}
                                    onHide={() => setEditPopup(false)}
                                    id={props.el.id}
                                    getBoards={() => props.getBoards()}
                                />
                            </div>
                        </Row>
                    </Card.Footer>
                </Card>

            </div>

        </>
    )
}

export default BoardView;