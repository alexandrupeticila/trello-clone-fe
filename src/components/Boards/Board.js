import React, {useEffect, useState} from 'react';
import axios from 'axios';
import NavBarLogged from "../NavBar/NavbarLogged";
import {Button, Card, Container, Form, Modal, Row} from "react-bootstrap";
import "./Board.css"
import CardsList from "./CardsList";

function AddCardsListPopup(props) {
    const [text, setText] = useState('');

    const onChangeText = event => {
        setText(event.target.value)
    }
    const addCardsList = (event) => {
        const token = localStorage.getItem("token");
        const cardsList = {
            name: text,
        }

        axios.post(`createList/${props.id}`, cardsList, {
            headers: {
                Authorization: `Bearer ${token}`
            }
        })
            .then(response => {
                props.getListCards()
                props.onHide();
            })
            .catch(error => {
                console.log(error);
                props.onHide();
            })
    }

    return (
        <>
            <Modal
                {...props}
                size="lg"
                aria-labelledby="contained-modal-title-vcenter"
                centered
            >
                <Modal.Header closeButton>
                    <Modal.Title id="contained-modal-title-vcenter" style={{alignItems: 'center'}}>
                        Give your list a name!
                    </Modal.Title>
                </Modal.Header>
                <Modal.Body>
                    <Form>
                        <Form.Group controlId="username">
                            <Form.Label>Name</Form.Label>
                            <Form.Control
                                type="text"
                                placeholder="list's name"
                                onChange={onChangeText}
                            />
                        </Form.Group>
                    </Form>
                </Modal.Body>
                <Modal.Footer>
                    <Button variant="info" onClick={addCardsList}>Create</Button>
                    <Button variant="danger" onClick={props.onHide}>Cancel</Button>
                </Modal.Footer>
            </Modal>
        </>
    );
}

function Board(props) {
    const [addListCardPopup, setAddListCardPopup] = useState(false)
    const [boardData, setBoardData] = useState([]);
    const [board, setBoard] = useState("");
    const {id} = props.match.params;

    const getListCards = () => {
        const token = localStorage.getItem("token");
        axios
            .get(`getCardsList/${id}`, {
                headers: {
                    Authorization: `Bearer ${token}`
                }
            })
            .then(response => {
                console.log(JSON.stringify(response))
                const jsonResponse = JSON.parse(JSON.stringify(response));
                setBoardData(jsonResponse.data);
            })
            .catch(error => console.log(error))
    }

    const getBoard = () => {
        const token = localStorage.getItem("token");

        axios
            .get(`getBoard/${id}`, {
                headers: {
                    Authorization: `Bearer ${token}`
                }
            })
            .then(response => {
                const jsonResponse = JSON.parse(JSON.stringify(response));
                setBoard(jsonResponse.data);
            })
            .catch(error => console.log(error))
    }
    useEffect(() => {
        getListCards();
        getBoard();
    }, [])

    const addListCard = () => {
        setAddListCardPopup(true)
    }


    return (
        (boardData === undefined) ?
            <div>
            </div>
            :
            <div>
                <NavBarLogged/>
                <AddCardsListPopup
                    show={addListCardPopup}
                    onHide={() => setAddListCardPopup(false)}
                    getListCards={() => getListCards()}
                    id={id}
                />
                <div>
                    <div className="board-name-row">
                        {board.name}
                    </div>
                    <Container className="card-list-container">
                        <Row className="card-list-row">
                            {boardData.map(el =>
                                <CardsList key={el.id}
                                           el={el}
                                           id={id}
                                           getListCards={() => getListCards()}
                                />
                            )}
                            <div className="cards-list-col">
                                <Card className="post-content-card" style={{
                                    width: "300px",
                                    backgroundColor: "lightgrey",
                                    height: "50px",
                                    margin: "20px 20px"
                                }} onClick={addListCard}>
                                    <i className="fas fa-plus plus-icon fa-2x"/>
                                </Card>
                            </div>
                        </Row>
                    </Container>
                </div>
            </div>
    )
}


export default Board;
