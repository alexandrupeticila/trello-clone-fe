import React, {useState} from 'react';
import axios from 'axios';
import {Button, Card, Form, Modal, Row} from "react-bootstrap";
import "./Board.css"
import MyCard from "./MyCard";

function AddCardPopup(props) {
    const [title, setTitle] = useState('');
    const [description, setDescription] = useState('');

    const onChangeText = event => {
        setTitle(event.target.value)
    }
    const onChangeDescription = event => {
        setDescription(event.target.value)
    }
    const addCardsList = (event) => {
        const token = localStorage.getItem("token");
        const card = {
            title: title,
            description: description,
        }

        axios.post(`/boards/createCard/${props.id}/${props.listId}`, card, {
            headers: {
                Authorization: `Bearer ${token}`
            }
        })
            .then(response => {
                props.getListCards()
                props.onHide();
            })
            .catch(error => {
                console.log(error);
                props.onHide();
            })
    }

    return (
        <>
            <Modal
                {...props}
                size="lg"
                aria-labelledby="contained-modal-title-vcenter"
                centered
            >
                <Modal.Header closeButton>
                    <Modal.Title id="contained-modal-title-vcenter" style={{alignItems: 'center'}}>
                        Give your card a title and a description!
                    </Modal.Title>
                </Modal.Header>
                <Modal.Body>
                    <Form>
                        <Form.Group controlId="username">
                            <Form.Label>Title</Form.Label>
                            <Form.Control
                                type="text"
                                placeholder="card's name"
                                onChange={onChangeText}
                            />
                        </Form.Group>
                        <Form.Group controlId="username">
                            <Form.Label>Description</Form.Label>
                            <Form.Control
                                as="textarea"
                                row="3"
                                placeholder="card's description"
                                onChange={onChangeDescription}
                            />
                        </Form.Group>
                    </Form>
                </Modal.Body>
                <Modal.Footer>
                    <Button variant="info" onClick={addCardsList}>Create</Button>
                    <Button variant="danger" onClick={props.onHide}>Cancel</Button>
                </Modal.Footer>
            </Modal>
        </>
    );
}

function EditCardListPopup(props) {
    const [text, setText] = useState('');

    const onChangeText = event => {
        setText(event.target.value)
    }

    const editCardsList = () => {
        const token = localStorage.getItem("token");
        const cardsList = {
            name: text,
        }

        axios.put(`/boards/editList/${props.id}/${props.listId}`, cardsList, {
            headers: {
                Authorization: `Bearer ${token}`
            }
        })
            .then(response => {
                props.getListCards()
                props.onHide();
            })
            .catch(error => {
                console.log(error);
                props.onHide();
            })
    }

    return (
        <>
            <Modal
                {...props}
                size="lg"
                aria-labelledby="contained-modal-title-vcenter"
                centered
            >
                <Modal.Header closeButton>
                    <Modal.Title id="contained-modal-title-vcenter" style={{alignItems: 'center'}}>
                        Edit the name of your cards list!
                    </Modal.Title>
                </Modal.Header>
                <Modal.Body>
                    <Form>
                        <Form.Group controlId="username">
                            <Form.Label>New name</Form.Label>
                            <Form.Control
                                type="text"
                                placeholder="list's new name"
                                onChange={onChangeText}
                            />
                        </Form.Group>
                    </Form>
                </Modal.Body>
                <Modal.Footer>
                    <Button variant="info" onClick={editCardsList}>Save</Button>
                    <Button variant="danger" onClick={props.onHide}>Cancel</Button>
                </Modal.Footer>
            </Modal>
        </>
    );
}

function CardsList(props) {
    const [editListCardPopup, setEditListCardPopup] = useState(false)
    const [addCardPopup, setAddCardPopup] = useState(false)

    const editListCard = () => {
        setEditListCardPopup(true);
    }
    const addCard = () => {
        setAddCardPopup(true)
    }
    const deleteListCard = (listId) => {
        const token = localStorage.getItem("token");
        axios.delete(`/boards/deleteList/${props.id}/${listId}`, {
            headers: {
                Authorization: `Bearer ${token}`
            }
        })
            .then(response => {
                props.getListCards();
            })
            .catch(error => {
                console.log(error);
            })
    }


    return (
        <div className="cards-list-col">
            <Card className="card-cards-list">
                <div>
                    <Card.Title>
                        <b><i>{props.el.name}</i></b>
                    </Card.Title>
                </div>
                <div>
                    {
                        props.el.cards.map(card =>
                            <MyCard
                                getListCards={() => props.getListCards()}
                                id={props.id}
                                listId={props.el.id}
                                cardId={card.id}
                                card={card}
                            />
                        )}
                </div>
                <Card.Footer className="cards-list-footer">
                    <Row>
                        <div className="trash-icon">
                            <i className="far fa-trash-alt"
                               onClick={() => deleteListCard(props.el.id)}/>
                        </div>

                        <div className="add-card-to-list">
                            <AddCardPopup
                                show={addCardPopup}
                                onHide={() => setAddCardPopup(false)}
                                getListCards={() => props.getListCards()}
                                id={props.id}
                                listId={props.el.id}
                            />
                            <i className="fas fa-plus plus-icon fa-2x"
                               onClick={addCard}/>
                        </div>
                        <div className="edit-icon">
                            <i className="far fa-edit" onClick={editListCard}/>
                            <EditCardListPopup
                                show={editListCardPopup}
                                onHide={() => setEditListCardPopup(false)}
                                getListCards={() => props.getListCards()}
                                id={props.id}
                                listId={props.el.id}
                            />
                        </div>
                    </Row>
                </Card.Footer>
            </Card>
        </div>
    )
}


export default CardsList;
