import React, {useCallback, useState} from 'react';
import {Button, Col, Form, FormGroup, Row} from "react-bootstrap";
import axios from "axios";
import Avatar from "avataaars";
import {history} from "../../_helpers";
import NavBar from '../NavBar/Navbar';
import './MyAvatar.css'

function MyAvatar(props) {
    const styles = ['Circle', 'Transparent']
    const tops = ['NoHair', 'LongHairBob', 'LongHairFro', 'LongHairStraight', 'ShortHairShortFlat'];
    const hairColors = ['Black', 'Blonde', 'Brown', 'Red']
    const facialHairs = ['Blank', 'BeardMedium', 'MustacheMagnum']
    const skins = ['Tanned', 'Pale', 'Light', 'Black']
    const clothes = ['BlazerShirt', 'Hoodie', 'ShirtScoopNeck', 'Overall']

    const [avatarStyle, setAvatarStyle] = useState("");
    const [top, setTop] = useState("");
    const [haiColor, setHairColor] = useState("");
    const [facialHair, setFacialHair] = useState("");
    const [skin, setSkin] = useState("");
    const [clothe, setClothe] = useState("");


    const onChangeAvatarStyle = useCallback((event) => {
        setAvatarStyle(event.target.value);
    }, []);
    const onChangeTop = useCallback((event) => {
        setTop(event.target.value);
    }, []);
    const onChangeHairColor = useCallback((event) => {
        setHairColor(event.target.value);
    }, []);
    const onChangeFacialHair = useCallback((event) => {
        setFacialHair(event.target.value);
    }, []);
    const onChangeSkin = useCallback((event) => {
        setSkin(event.target.value);
    }, []);
    const onChangeClothe = useCallback((event) => {
        setClothe(event.target.value);
    }, []);

    const createProfile = (e) => {
        e.preventDefault();
        const url = "http://avataaars.io/?avatarStyle=" + avatarStyle +
            "&topType=" + top +
            "&accessoriesType=Blank&hairColor=" + haiColor +
            "&facialHairType=" + facialHair +
            "&clotheType=" + clothe +
            "&clotheColor=PastelBlue&eyeType=Default&eyebrowType=Default&mouthType=Smile" +
            "&skinColor=" + skin;

        const token = localStorage.getItem("token");

        const req = {
            link: url
        }
        axios.post(`/user/saveImage`, req, {
            headers: {
                Authorization: `Bearer ${token}`
            }
        })
            .then(response => {
                const jsonResponse = JSON.parse(JSON.stringify(response));
                history.push('/boards');
                window.location.reload();
            })
            .catch(error => {
                const errorMessage = JSON.stringify(error.response.data.message);
                console.log(errorMessage)
            })


    }

    return (

        <div className="avatar-container">
            <NavBar/>
            <h2 className="last-step">One last step, create your avatar</h2>
            <div
                className="avatar-icon">
                <Avatar
                    className="avatar-icon"
                    avatarStyle={avatarStyle}
                    topType={top}
                    hairColor={haiColor}
                    facialHairType={facialHair}
                    clotheType={clothe}
                    clotheColor='PastelBlue'
                    eyeType='Default'
                    eyebrowType='Default'
                    mouthType='Smile'
                    skinColor={skin}
                />
            </div>


            <Form className='avatarForm'>
                <Row>
                    <Col className='avatar-type' md={{offset: 2, span: 2}} xs={{offset: 2, span: 3}}>
                        <div>
                            Style
                        </div>
                    </Col>
                    <Col md={6} xs={5}>
                        <FormGroup>
                            <Form.Control
                                as="select"
                                custom
                                onChange={onChangeAvatarStyle}
                                className="input-text"
                            >
                                <option value="" disabled selected hidden>
                                    Style
                                </option>
                                {styles.map((name, index) => (
                                    <option key={index}>{name}</option>
                                ))}
                            </Form.Control>

                        </FormGroup>
                    </Col>
                </Row>
                <Row>
                    <Col className='avatar-type' md={{offset: 2, span: 2}} xs={{offset: 2, span: 3}}>
                        <div>
                            Skin Color
                        </div>
                    </Col>
                    <Col md={6} xs={5}>
                        <FormGroup>
                            <Form.Control
                                as="select"
                                custom
                                onChange={onChangeSkin}
                                className="input-text"
                            >
                                <option value="" disabled selected hidden>
                                    Please select skin color
                                </option>
                                {skins.map((name, index) => (
                                    <option key={index}>{name}</option>
                                ))}
                            </Form.Control>

                        </FormGroup>
                    </Col>
                </Row>
                <Row>
                    <Col className='avatar-type' md={{offset: 2, span: 2}} xs={{offset: 2, span: 3}}>
                        <div>
                            Hair
                        </div>
                    </Col>
                    <Col md={6} xs={5}>
                        <FormGroup>
                            <Form.Control
                                as="select"
                                custom
                                onChange={onChangeTop}
                                className="input-text"
                            >
                                <option value="" disabled selected hidden>
                                    Hair
                                </option>
                                {tops.map((name, index) => (
                                    <option key={index}>{name}</option>
                                ))}
                            </Form.Control>

                        </FormGroup>
                    </Col>
                </Row>
                <Row>
                    <Col className='avatar-type' md={{offset: 2, span: 2}} xs={{offset: 2, span: 3}}>
                        <div>
                            Hair Color
                        </div>
                    </Col>
                    <Col md={6} xs={5}>
                        <FormGroup>
                            <Form.Control
                                as="select"
                                custom
                                onChange={onChangeHairColor}
                                className="input-text"
                            >
                                <option value="" disabled selected hidden>
                                    Hair color
                                </option>
                                {hairColors.map((name, index) => (
                                    <option key={index}>{name}</option>
                                ))}
                            </Form.Control>

                        </FormGroup>
                    </Col>
                </Row>
                <Row>
                    <Col className='avatar-type' md={{offset: 2, span: 2}} xs={{offset: 2, span: 3}}>
                        <div>
                            Facial Hair
                        </div>
                    </Col>
                    <Col md={6} xs={5}>
                        <FormGroup>
                            <Form.Control
                                as="select"
                                custom
                                onChange={onChangeFacialHair}
                                className="input-text"
                            >
                                <option value="" disabled selected hidden>
                                    Facial hair
                                </option>
                                {facialHairs.map((name, index) => (
                                    <option key={index}>{name}</option>
                                ))}
                            </Form.Control>

                        </FormGroup>
                    </Col>
                </Row>
                <Row>
                    <Col className='avatar-type' md={{offset: 2, span: 2}} xs={{offset: 2, span: 3}}>
                        <div>
                            Clothes
                        </div>
                    </Col>
                    <Col md={6} xs={5}>
                        <FormGroup>
                            <Form.Control
                                as="select"
                                custom
                                onChange={onChangeClothe}
                                className="input-text"
                            >
                                <option value="" disabled selected hidden>
                                    Clothes
                                </option>
                                {clothes.map((name, index) => (
                                    <option key={index}>{name}</option>
                                ))}
                            </Form.Control>

                        </FormGroup>
                    </Col>
                </Row>
            </Form>
            <div className="text-center pt-4">
                <Button variant="info" type="submit" onClick={createProfile} size="lg" className="create-avatar">
                    Create your avatar!
                </Button>
            </div>
        </div>
    )
}

export default MyAvatar;