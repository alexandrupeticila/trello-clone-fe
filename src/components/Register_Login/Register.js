import React, {useState, useEffect} from "react";
import {Form, Button, Row, Col} from "react-bootstrap";
import {Link} from "react-router-dom";
import {useDispatch, useSelector} from 'react-redux';
import {ToastContainer} from "react-toastify";
import './Login_Register.css'
import {userActions} from '../../_actions';

function Register() {
    const [validated, setValidated] = useState(false);
    const [firstName, setFirstName] = useState("");
    const [lastName, setLastName] = useState("");
    const [email, setEmail] = useState("");
    const [password, setPassword] = useState("");

    const registering = useSelector(state => state.registration.registering);
    const dispatch = useDispatch();

    // reset login status
    useEffect(() => {
        dispatch(userActions.logout());
    }, [dispatch]);


    function onChangeFirstName(e) {
        setFirstName(e.target.value)
    }

    function onChangeLastName(e) {
        setLastName(e.target.value)
    }

    function onChangeEmail(e) {
        setEmail(e.target.value)
    }


    function onChangeUserPassword(e) {
        setPassword(e.target.value)
    }

    function onSubmit(e) {
        setValidated(false);
        e.preventDefault();

        const form = e.currentTarget;
        if (form.checkValidity() === false) {
            e.stopPropagation();
        }
        setValidated(true);

        console.log(validated + "validated");
        if (validated) {
            const userObject = {
                firstName: firstName,
                lastName: lastName,
                email: email,
                password: password
            };
            console.log(userObject)
            dispatch(userActions.register(userObject));
        }
    }

    return (
        <div
            className='card card-form'
            id='user-card'
            style={{borderRadius: '25px'}}
        >
            <ToastContainer
                position="top-center"
                autoClose={2000}
                hideProgressBar={false}
                newestOnTop
                closeOnClick
                rtl={false}
                draggable
                pauseOnHover
            />
            <div
                className='card-body'
                style={{
                    backgroundColor: '#343a40',
                    color: 'white',
                    borderRadius: '25px',
                }}
            >
                <Form noValidate validated={validated} onSubmit={onSubmit}>
                    <h3 className='text-center'>Sign Up Today</h3>
                    <p className='text-center'>Please fill out this form to register</p>
                    <Row>
                        <Col>
                            <Form.Group controlId='username'>
                                <Form.Label>First Name</Form.Label>
                                <Form.Control
                                    required
                                    autoFocus
                                    type='text'
                                    placeholder='FirstName'
                                    onChange={onChangeFirstName}
                                />
                                <Form.Control.Feedback type='invalid'>
                                    Please enter your firstname!
                                </Form.Control.Feedback>
                            </Form.Group>
                        </Col>
                        <Col>
                            <Form.Group controlId='username'>
                                <Form.Label>Last Name</Form.Label>
                                <Form.Control
                                    required
                                    type='text'
                                    placeholder='LastName'
                                    onChange={onChangeLastName}
                                />
                                <Form.Control.Feedback type='invalid'>
                                    Please enter your lastname!
                                </Form.Control.Feedback>
                            </Form.Group>
                        </Col>
                    </Row>
                    <Form.Group controlId='username'>
                        <Form.Label>Email</Form.Label>
                        <Form.Control
                            required
                            type='email'
                            placeholder='example@office.com'
                            onChange={onChangeEmail}
                        />
                        <Form.Control.Feedback type='invalid'>
                            Please enter your email address!
                        </Form.Control.Feedback>
                    </Form.Group>
                    <Form.Group controlId='password'>
                        <Form.Label>Password (minimum 8 characters)</Form.Label>
                        <Form.Control
                            required
                            type='password'
                            placeholder='password'
                            onChange={onChangeUserPassword}
                            minLength='8'
                        />
                        <Form.Control.Feedback type='invalid'>
                            Please choose a password with minimum 8 characters!
                        </Form.Control.Feedback>
                    </Form.Group>
                    <Form.Group>
                        <Link to='/gdpr' target='_blank'>
                            <Form.Check
                                style={{color: 'white'}}
                                size='lg'
                                required
                                label='Agree to terms and conditions'
                                feedback='You must agree before submitting.'
                            />
                        </Link>
                    </Form.Group>
                    <Button color='#17a2b8' variant="outline-info" type='submit'>
                        {registering && (
                            <span className='spinner-border spinner-border-sm mr-1'></span>
                        )}
                        Sign Up
                    </Button>
                    <Link to='/login'>
                        <p
                            className='forgot-password text-right'
                            style={{color: 'white'}}
                        >
                            Already registered? Sign In
                        </p>
                    </Link>
                </Form>
            </div>
        </div>
    );
}

export default Register;
