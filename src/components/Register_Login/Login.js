import React, {useState, useEffect} from "react";
import {Form, Button} from "react-bootstrap";
import {Link} from "react-router-dom";
import {useDispatch} from "react-redux";
import {userActions} from "../../_actions"
import {ToastContainer} from "react-toastify";
import './Login_Register.css';

function Login(props) {
    const [email, setEmail] = useState("");
    const [password, setPassword] = useState("");
    const dispatch = useDispatch();

    function onChangeUserName(e) {
        setEmail(e.target.value)
    }

    function onChangeUserPassword(e) {
        setPassword(e.target.value)
    }

    // reset login status
    useEffect(() => {
        dispatch(userActions.logout());
    }, [dispatch]);

    function onSubmit(e) {
        e.preventDefault();


        const userObject = {
            email: email,
            password: password
        };
        dispatch(userActions.login(userObject));
    }


    return (
        <div
            className='card card-form mt-5'
            id='user-card'
            style={{borderRadius: '25px'}}
        >
            <ToastContainer
                position="top-center"
                autoClose={2000}
                hideProgressBar={false}
                newestOnTop
                closeOnClick
                rtl={false}
                draggable
                pauseOnHover
                className={type => type === "error" ? "bg-blue" : "bg-green"}
            />
            <div
                className='card-body'
                style={{
                    backgroundColor: '#343a40',
                    color: 'white',
                    borderRadius: '25px',
                }}
            >
                <h3 className='text-center'>Sign In</h3>
                <p className='text-center'>Please enter your email and password</p>
                <Form onSubmit={onSubmit}>
                    <Form.Group controlId='username'>
                        <Form.Label className='p-2'>
                            <strong>Email</strong>
                        </Form.Label>
                        <Form.Control
                            required
                            size='lg'
                            autoFocus
                            type='username'
                            placeholder='email'
                            onChange={onChangeUserName}
                        />
                    </Form.Group>
                    <Form.Group controlId='password'>
                        <Form.Label className='p-2'>
                            <strong>Password</strong>
                        </Form.Label>
                        <Form.Control
                            required
                            size='lg'
                            type='password'
                            placeholder='Password'
                            onChange={onChangeUserPassword}
                        />
                    </Form.Group>

                    <Link to='/boards'>
                        <div className='text-center pt-4'>
                            <Button
                                color="#17a2b8"
                                variant="outline-info"
                                type='submit'
                                onClick={onSubmit}
                                className='text-center'
                            >
                                Login
                            </Button>
                        </div>
                    </Link>

                    <Link to='/register'>
                        <h5 className='text-right pt-4' style={{color: 'white'}}>
                            Don't have an account? Sign Up
                        </h5>
                    </Link>
                </Form>
            </div>
        </div>
    );
}

export default Login;