import React from "react";
import NavBar from '../NavBar/Navbar'
import { Container, Col } from 'react-bootstrap'

export default function gdpr() {
    return (
        <>
            <NavBar />
            <h1 className="large text-info text-center p-4"> Confidential policy</h1>
            <Container className="m-4">
                <header style={{ color: "red" }} className="text-right p-4">Lorem Ipsum</header>
                <Col lg={{ span: 10, offset: 4 }}>
                    Lorem ipsum dolor sit amet consectetur adipisicing elit. Fuga nobis ea molestias ipsum unde laboriosam quas odio quidem, vero id quo temporibus, quis debitis earum soluta dicta enim culpa ipsam.
                    Lorem ipsum dolor sit, amet consectetur adipisicing elit. Fugiat aut delectus magni voluptates harum illum natus in, ab, inventore quod quasi quas expedita odit earum nulla. Quo aliquid corporis laboriosam!
                    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Maiores, ullam dolorem. Nulla ipsa deserunt tenetur amet quos assumenda repellat cum, maxime veritatis, at, animi rerum illo! Dolores fugit sapiente autem!
                    Lorem ipsum dolor, sit amet consectetur adipisicing elit. Accusamus nam voluptatem magni qui ratione ipsa dicta voluptates quas perferendis harum laboriosam cupiditate quidem natus itaque, praesentium eum quisquam! Aliquam, sequi.
                    Lorem ipsum dolor sit amet consectetur adipisicing elit. Laudantium omnis quaerat totam eum, minus quidem, reiciendis neque animi recusandae ea vero corporis blanditiis libero deleniti culpa nesciunt voluptatibus reprehenderit optio.
                    Lorem ipsum dolor sit amet consectetur adipisicing elit. Voluptas veniam veritatis dolorum autem eaque. A obcaecati error voluptatum dolorum aliquid, ipsa veniam sint distinctio enim tenetur modi pariatur id est?
                    </Col>
            </Container>
        </>
    );
}
