import React, {useState, useEffect} from "react";
import {Container, Button, Modal, Form, Row, Col} from "react-bootstrap";
import axios from 'axios';
import NavBarLogged from '../NavBar/NavbarLogged'
import './Profile.css'
import {toast, ToastContainer} from "react-toastify";


function ChangeName(props) {

    function updateUser() {
        toast.error("Feature not implemented")
        props.onHide(false);
    }

    useEffect(() => {
    }, [])
    return (
        <Modal
            {...props}
            size="lg"
            aria-labelledby="contained-modal-title-vcenter"
            centered
        >
            <Modal.Header closeButton>
                <Modal.Title id="contained-modal-title-vcenter">
                    Change your name
                </Modal.Title>
            </Modal.Header>
            <Modal.Body>
                <p>
                    Please fill in the following fields
                </p>
                <Form>
                    <Form.Label>New first name</Form.Label>
                    <Form.Control type="text" />
                    <Form.Label>New last name</Form.Label>
                    <Form.Control type="username"/>
                </Form>
            </Modal.Body>
            <Modal.Footer>
                <Button variant="info" onClick={updateUser}>Update</Button>
                <Button variant="danger" onClick={props.onHide}>Cancel</Button>
            </Modal.Footer>
        </Modal>
    );
}

export default function Profile(props) {

    const [user, setUser] = useState("");
    const [photo, setPhoto] = useState('');
    const [updateName, setUpdateName] = useState(false);

    function getUser() {
        const token = localStorage.getItem("token");
        axios.get(`/user/profile`, {
            headers: {
                Authorization: `Bearer ${token}`
            }
        })
            .then(response => {
                const jsonResponse = JSON.parse(JSON.stringify(response));
                setUser(jsonResponse.data)
                console.log(user)
            })
            .catch(error => {
                //alert(error);
            })

        axios.get(`/user/profile/photo`, {
            headers: {
                Authorization: `Bearer ${token}`
            }
        })
            .then(response => {
                const base64Image = response.data;
                console.log(response.data)
                setPhoto(base64Image)
            })
            .catch(error => {
                //alert(error);
            })

    }

    useEffect(() => {
        getUser();
    }, [])

    return (
        (!photo || !user) ?
            <div></div>
            :
            <>
                <NavBarLogged/>
                <ToastContainer
                    position="top-center"
                    autoClose={2000}
                    hideProgressBar={false}
                    newestOnTop
                    closeOnClick
                    rtl={false}
                    draggable
                    pauseOnHover
                />
                <Container className="">
                    <Row>

                        <Col>
                            <br/>
                            <br/>
                            <h3><br/></h3>

                            <h1 className="large text-info p-2"> Personal Info</h1>
                            <br/>
                            <br/>
                            <h3><i className="fas fa-user"/>{' '}
                                {user.firstName} {user.lastName} {' '}<i
                                    className="far fa-edit" onClick={() => setUpdateName(true)}/></h3>
                            <h4><i className="fas fa-envelope py-2"/> Email : {user.email}{' '}</h4>
                            <h2><br/></h2>

                        </Col>
                        <Col className="d-lg-block profile-photo">
                            <br/>
                            <br/>
                            <img src={`data:image/svg+xml;utf8,${encodeURIComponent(photo)}`} alt={""}/>
                            <br/>
                            <br/>
                        </Col>
                        < ChangeName
                            show={updateName}
                            onHide={() => setUpdateName(false)}
                            post={props.post}/>

                    </Row>
                </Container>
            </>
    )
}