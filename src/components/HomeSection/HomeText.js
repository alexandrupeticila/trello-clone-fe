import React from 'react';
import './HomeText.css'

export default function HomeText() {
    return (
        <div id="homeText" className="text-center d-none d-md-table-cell">
            <h1>Lorem ipsum
                <strong> lorem ipsum</strong> lorem
                <strong> ipsum ipsum</strong>
            </h1>
            <div className="d-flex">
                <div className="p-4 align-self-start">
                    <i className="fas fa-check fa-2x"></i>
                </div>
                <div className="p-4 align-self-end">
                    Lorem ipsum dolor sit amet, consectetur adipisicing elit.

                </div>
            </div>

            <div className="d-flex">
                <div className="p-4 align-self-start">
                    <i className="fas fa-check fa-2x"></i>
                </div>
                <div className="p-4 align-self-end">
                    Lorem ipsum dolor sit amet, consectetur adipisicing elit.rio.

                </div>
            </div>
        </div>

    )
}