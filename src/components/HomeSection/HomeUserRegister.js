import React from 'react';
import { Container, Row, Col } from 'react-bootstrap'
import Register from '../Register_Login/Register';
import NavBar from '../NavBar/Navbar';
import './HomeSection.css'

export default function HomeUserRegister() {
    return (
        <div id="homeContainerRegister">
            <div className="dark-overlay">
                <NavBar />
                <Container className="mb-4">
                    <Row id="rowHomeSection" >
                        <Col lg={{ span: 6, offset: 3 }} className="mt-5">
                            <Register />
                        </Col>
                    </Row>
                </Container>
            </div>
        </div>
    )
}