import React from 'react';
import { Container, Row, Col } from 'react-bootstrap'
import Login from '../Register_Login/Login';
import NavBar from '../NavBar/Navbar';
import HomeText from './HomeText'
import './HomeSection.css'
import { ToastContainer } from 'react-toastify';

export default function HomeUserLogin() {
    return (
        <div id="homeContainerRegister">
            <div className="dark-overlay">
                <NavBar />
                <Container>
                    <ToastContainer
                        position="top-center"
                        autoClose={2000}
                        hideProgressBar={false}
                        newestOnTop
                        closeOnClick
                        rtl={false}
                        draggable
                        pauseOnHover
                    />
                    <Row id="rowHomeSection" >
                        <Col className="" lg={{ span: 7, offset: 0 }}>
                            <HomeText className="text-info" />
                        </Col>
                        <Col lg={{ span: 4, offset: 1 }} className="mt-2">
                            <Login />
                        </Col>
                    </Row>
                </Container>
            </div>
        </div>
    )
}