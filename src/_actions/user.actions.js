import {userConstants} from '../_constants';
import {userService} from '../_services';
import {history} from '../_helpers';
import {toast} from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';

export const userActions = {
    login,
    logout,
    register,
};

function login(userObject) {
    return dispatch => {
        const username = userObject.username;
        dispatch(request({username}));

        userService.login(userObject)
            .then(
                user => {
                    if (localStorage.getItem("hasProfile") === "false") {
                        dispatch(successWithoutProfile(user))
                        history.push('/avatar');
                    } else {
                        dispatch(success(user));
                        history.push('/boards');
                    }
                    window.location.reload(false);
                },
                error => {
                    const errorMessage = JSON.stringify(error.response.data.message);
                    toast.error(errorMessage);
                    dispatch(failure(errorMessage));
                }
            );
    };

    function successWithoutProfile(user) {
        return {type: userConstants.LOGIN_SUCCESS_WP, user}
    }

    function request(user) {
        return {type: userConstants.LOGIN_REQUEST, user}
    }

    function success(user) {
        return {type: userConstants.LOGIN_SUCCESS, user}
    }

    function failure(error) {
        return {type: userConstants.LOGIN_FAILURE, error}
    }
}

function logout() {
    userService.logout();
    return {type: userConstants.LOGOUT};
}

function register(user) {
    return dispatch => {
        dispatch(request(user));

        userService.register(user)
            .then(
                user => {
                    dispatch(success());
                    history.push('/login');
                    window.location.reload(false);
                },
                error => {
                    const errorMessage = JSON.stringify(error.response.data.message);
                    toast.error(errorMessage);
                    dispatch(failure(errorMessage));
                }
            );
    };

    function request(user) {
        return {type: userConstants.REGISTER_REQUEST, user}
    }

    function success(user) {
        return {type: userConstants.REGISTER_SUCCESS, user}
    }

    function failure(error) {
        return {type: userConstants.REGISTER_FAILURE, error}
    }
}
