import axios from 'axios';

export const userService = {
    login,
    logout,
    register,
    addNewCase
};

function login(user) {
    return axios.post(`/user/login`, user)
        .then(response => {
            const jsonResponse = JSON.parse(JSON.stringify(response))
            console.log(jsonResponse.data)
            localStorage.setItem("token", jsonResponse.data.jwt)
            localStorage.setItem("hasProfile", jsonResponse.data.hasProfile)
        })
}

function logout() {
    // remove user from local storage to log user out
    localStorage.removeItem('user');
}
function register(user) {
    return axios.post(`/user/add`, user);
}

function setLocation(location) {
    const token = localStorage.getItem("token");
    return axios.post(`/user/createProfile`, location, {
        headers: {
            Authorization: `Bearer ${token}`,
        }
    })
}

function addNewCase(newCase) {
    const token = localStorage.getItem("token");
    return axios.post(`/permanentCase/add`, newCase, {
        headers: {
            Authorization: `Bearer ${token}`,
        }
    })
}