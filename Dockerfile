# pull official base image
FROM node:14.15.1-alpine3.10
MAINTAINER alexandru.peticila@yahoo.com
WORKDIR /app

COPY package.json /app
COPY . /app
RUN npm install
EXPOSE 3000
CMD ["npm", "start"]